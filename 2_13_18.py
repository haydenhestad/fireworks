# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 14:46:27 2019

@author: Student
"""
import pygame
from vector2 import vector2
import random

class Particle:
    def __init__(self, mass, pos, vel=vector2(0,0), gravity=vector2(0,0)):
        if mass == 0:
            self.invmass = 0
        else:
            self.invmass = 1.0/mass
        self.pos = vector2(pos)
        self.vel = vector2(vel)
        self.gforce = mass*gravity
        self.force = self.gforce
        
    def integrate(self, dt):
        self.vel += self.invmass*self.force*dt
        self.pos += self.vel*dt
        self.force = self.gforce
    

class Circle(Particle):
    def __init__(self, mass, pos, vel, radius, color=(0,0,0), gravity=vector2(0,0)):
        super().__init__(mass, pos, vel, gravity)
        self.radius = radius
        self.color = color
        
    def draw(self, screen):
        pygame.draw.circle(screen, self.color, self.pos.pygame(), int(self.radius+0.5))

def main():
    pygame.init()
    clock = pygame.time.Clock()
    fps = 60
    size = [300, 300]
    bg = [255, 255, 255]
    
    screen = pygame.display.set_mode(size)
    
    ball = []
    
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return False
            
            if event.type == pygame.MOUSEBUTTONUP:

                mouse_pos = event.pos
                random_number = random.randint(15,30)
                for i in range(random_number):
                    mass = random.randint(5, 50)
                    vel = vector2(random.randint(-100,100),random.randint(-100,100))
                    rad = random.randint(0,5)
                    color = (random.randint(0,255),random.randint(0,255),random.randint(0,255))
                    ball.append(Circle(mass, mouse_pos, vel, rad, color, vector2(0.0,9.8)))
                
            
        screen.fill(bg)
        for x in ball:
            x.draw(screen)
        for x in ball:
            x.integrate(1/fps)
        

        pygame.display.update()
        clock.tick(fps)


main()
pygame.quit()
        
        
        
    
    